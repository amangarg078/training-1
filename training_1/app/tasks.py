from __future__ import absolute_import
import os
import redis
import requests
from celery import Celery

from celery.decorators import task
from celery.utils.log import get_task_logger


logger = get_task_logger(__name__)

@task(name="requests_to_nyt")
def requests_to_nyt(url):
    """sends a request to given url"""
    logger.info("Sent request to {}".format(url))
    print "Sent request to {}".format(url)
    response = requests.get(url)
    return response.status_code
