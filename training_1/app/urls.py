from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^celery/', views.RequestsViewWithCelery.as_view(),name='celery'),
    url(r'^view/', views.RequestsView.as_view(),name='index'),
]