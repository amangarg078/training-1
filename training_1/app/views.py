import requests

from django.shortcuts import render
from django.utils import timezone
from django.views.generic import TemplateView

from .tasks import requests_to_nyt

class RequestsView(TemplateView):
    """
    A view that will trigger 10 requests and note the time taken to complete all the requests
    """
    template_name = "app/view.html"

    def get_context_data(self, **kwargs):
        context = super(RequestsView, self).get_context_data(**kwargs)
        result_status_code = []
        url = 'https://www.nytimes.com/'
        start_time = timezone.now()

        for i in range(0,10):
            response = requests.get(url)

            if response.status_code == 200:
                result_status_code.append(response.status_code)

        elasped_time = timezone.now() - start_time
        if len(result_status_code) != 10:
            context['error'] = "Something went wrong"

        context['start_time'] = start_time
        context['elasped_time'] = elasped_time
        return context


class RequestsViewWithCelery(TemplateView):

    """
    A view that will trigger a celery task which will send 10 request
    """

    template_name = "app/view_celery.html"

    def get_context_data(self, **kwargs):
        context = super(RequestsViewWithCelery, self).get_context_data(**kwargs)
        result_status_code = []
        url = 'https://www.nytimes.com/'
        start_time = timezone.now()

        for i in range(0,10):
            #trigger the task
            response_status_code = requests_to_nyt.delay(url)
            if response_status_code == 200:
                result_status_code.append(response_status_code)

        elasped_time = timezone.now() - start_time
        if len(result_status_code) != 10:
            context['error'] = "Something went wrong"

        context['start_time'] = start_time
        context['elasped_time'] = elasped_time
        return context
