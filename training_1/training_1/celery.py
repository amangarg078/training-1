from __future__ import absolute_import

import os
import redis
from celery import Celery
from django.conf import settings



# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'training_1.settings')

REDIS_CLIENT = redis.Redis()
app = Celery('training_1',broker=settings.BROKER_URL, backend=settings.BROKER_URL)

app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)